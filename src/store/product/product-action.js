import * as Type from './product-types';

import { callApi } from "../../services/api-config";
import { PRODUCT, PRODUCT_BY_ID } from "../../services/api-link";

export const loadAllProducts = (page) => {
  return dispatch => {
    dispatch({ type: Type.PRODUCT_REQUEST })
    callApi(`${PRODUCT}?page=${page}`, "GET")
      .then(result => dispatch({
        type: Type.GET_PRODUCTS_SUCCESS,
        products: result.products,
        totalPage: result.totalPage,
        currentPage: result.currentPage,
        prodsPerPage: result.prodsPerPage,
      }))
      .catch(error => dispatch({ type: Type.PRODUCT_FAILURE, error: error }))
  }
}

export const loadSingleProduct = (ObjectId) => {
  return dispatch => {
    dispatch({ type: Type.PRODUCT_REQUEST })
    callApi(`${PRODUCT_BY_ID(ObjectId)}`, "GET")
      .then(result => dispatch({ type: Type.GET_SINGLE_PRODUCT_SUCCESS, payload: result }))
      .catch(error => dispatch({ type: Type.PRODUCT_FAILURE, error: error }))
  }
}

export const createProduct = (newProd) => {
  return dispatch => {
    dispatch({ type: Type.PRODUCT_REQUEST })
    callApi(`${PRODUCT}`, "POST", newProd)
      .then(dispatch({ type: Type.CREATE_PRODUCT_SUCCESS }))
      .catch(error => dispatch({ type: Type.PRODUCT_FAILURE, error: error }))
  }
}

export const updateProduct = (updateProd) => {
  return dispatch => {
    dispatch({ type: Type.PRODUCT_REQUEST })
    callApi(`${PRODUCT_BY_ID(updateProd._id)}`, "PUT", updateProd)
      .then(result => {
        result !== null ?
          dispatch({ type: Type.UPDATE_PRODUCT_SUCCESS })
          :
          dispatch({ type: Type.PRODUCT_FAILURE, error: `Product ID: ${updateProd._id} doesn't exists!` })
      })
      .then(_ => loadAllProducts())
      .catch(error => dispatch({ type: Type.PRODUCT_FAILURE, error: error }))
  }
}

export const deleteProduct = (_id) => {
  return dispatch => {
    dispatch({ type: Type.PRODUCT_REQUEST })
    callApi(`${PRODUCT_BY_ID(_id)}`, "DELETE")
      .then(result => {
        result !== null ?
          dispatch({ type: Type.DELETE_PRODUCT_SUCCESS })
          :
          dispatch({ type: Type.PRODUCT_FAILURE, error: `Product ID: ${_id} doesn't exists!` })
      })
      .catch(error => dispatch({ type: Type.PRODUCT_FAILURE, error: error }))
  }
}

export const setCurrentProduct = (item) => {
  return dispatch => dispatch({ type: Type.SET_CURRENT_PRODUCT, payload: item })
}

export const setCreateModalState = () => {
  return dispatch => dispatch({ type: Type.SET_CREATE_MODAL_STATE });
}

export const setEditModalState = () => {
  return dispatch => dispatch({ type: Type.SET_EDIT_MODAL_STATE });
}

export const setDeleteModalState = () => {
  return dispatch => dispatch({ type: Type.SET_DELETE_MODEL_STATE });
}