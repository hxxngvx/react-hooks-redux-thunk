import React from 'react';
import './App.css';
import ProductList from './components/product/product';

function App() {
  return (
    <div className="App">
        <ProductList />
    </div>
  );
}

export default App;
