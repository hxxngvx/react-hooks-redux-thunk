import React, { useEffect } from 'react';
import { loadAllProducts, setCreateModalState, setEditModalState, setDeleteModalState, setCurrentProduct } from '../../store/product/product-action';
import {
    Typography,
    Container,
    Card,
    CardContent,
    CardActions,
    Button,
    ButtonGroup,
} from '@material-ui/core';
import { EditOutlined, DeleteOutline, CreateOutlined } from '@material-ui/icons';
import { useSelector, useDispatch } from 'react-redux';
import { ProductCreate } from './product-create';
import { ProductEdit } from './product-edit';
import { ProductDelete } from './product-delete';
import { ProductPaginate } from './product-pagination';

import './product.scss';

const ProductPage = () => {
    const state = useSelector(state => state.productReducer);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(loadAllProducts())
    }, [])

    const handleCreateProduct = _ => {
        dispatch(setCreateModalState())
    }

    const handleEditModal = item => {
        dispatch(setCurrentProduct(item))
        dispatch(setEditModalState())
    }

    const handleDeleteModal = item => {
        dispatch(setCurrentProduct(item))
        dispatch(setDeleteModalState())
    }

    const handleProducts = (products) => {
        return products && products.map((item, index) => {
            return <Card key={index} className="product-card" variant="outlined">
                <CardContent className="product-card__content">
                    <Typography gutterBottom variant="h5" component="h2">
                        {`Title: ${item.name}`}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {`Description: ${item.description}`}
                    </Typography>
                </CardContent>
                <CardActions className="product-card__actions">
                    <ButtonGroup>
                        <Button
                            variant="contained"
                            color="primary"
                            startIcon={<EditOutlined />}
                            onClick={() => handleEditModal(item)}
                        >
                            Edit
                    </Button>
                        <Button
                            variant="contained"
                            color="secondary"
                            startIcon={<DeleteOutline />}
                            onClick={() => handleDeleteModal(item)}
                        >
                            Delete
                    </Button>
                    </ButtonGroup>
                </CardActions>
            </Card>
        })
    }

    return (
        <Container className="product-container">
            <Typography className="product-container__title" variant="h2" component="h2">Example Product</Typography>
            <Button
                className="product-container__create-btn"
                variant="contained"
                color="default"
                startIcon={<CreateOutlined />}
                onClick={() => handleCreateProduct()}>
                Create
            </Button>
            {!state.loading && handleProducts(state.products)}

            <ProductCreate />
            <ProductEdit />
            <ProductDelete />

            <ProductPaginate />
        </Container>
    )
}

export default ProductPage;