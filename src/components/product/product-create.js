import React, { useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Dialog, DialogTitle, DialogContent, TextField, DialogActions, Button } from '@material-ui/core';
import { setCreateModalState, loadAllProducts, createProduct } from '../../store/product/product-action';

export const ProductCreate = () => {
    const state = useSelector(state => state.productReducer)
    const dispatch = useDispatch();

    const productNameRef = useRef()
    const productDescriptionRef = useRef()

    const handleSubmit = e => {
        e.preventDefault()
        const productObj = {
            name: productNameRef.current.value,
            description: productDescriptionRef.current.value
        }

        dispatch(createProduct(productObj))
        dispatch(loadAllProducts())
        dispatch(setCreateModalState())
    }

    return (
        <Dialog
            key="create-product"
            open={state.isCreateOpen}
            onClose={() => dispatch(setCreateModalState())}
            aria-labelledby="form-dialog-title"
            className="create-dialog">
            <DialogTitle className="create-dialog__title">Create product</DialogTitle>
            <form autoComplete="off">
                <DialogContent className="create-dialog__content">
                    <TextField
                        inputRef={productNameRef}
                        id="name"
                        label="Name"
                        type="text"
                        variant="outlined"
                        margin="dense"
                        required
                        autoFocus
                        fullWidth
                    />
                    <TextField
                        inputRef={productDescriptionRef}
                        variant="outlined"
                        margin="dense"
                        id="description"
                        label="Description"
                        type="text"
                        fullWidth
                        multiline
                        rows="4"
                    />
                </DialogContent>
                <DialogActions className="create-dialog__actions">
                    <Button onClick={() => dispatch(setCreateModalState())} color="primary">
                        Cancel
                </Button>
                    <Button color="primary" onClick={e => handleSubmit(e)}>
                        Create
                </Button>
                </DialogActions>
            </form>
        </Dialog>
    )
}