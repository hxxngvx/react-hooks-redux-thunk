import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ButtonGroup, Button } from '@material-ui/core';
import { loadAllProducts } from '../../store/product/product-action';

export const ProductPaginate = () => {
    const state = useSelector(state => state.productReducer)
    const dispatch = useDispatch()

    const handleChangePage = number => {
        dispatch(loadAllProducts(number))
    }

    return (
        <ButtonGroup className="product-container__paginate">
            <Button
                className="product-container__paginate--previous"
                disabled={state.currentPage === 1 ? true : false}
                variant="outlined"
                onClick={() => handleChangePage(state.currentPage - 1)}>
                Previous
            </Button>
            <Button
                className="product-container__paginate--next"
                disabled={state.currentPage === state.totalPage ? true : false}
                variant="outlined"
                onClick={() => handleChangePage(state.currentPage + 1)}>
                Next
            </Button>
        </ButtonGroup>
    )
}